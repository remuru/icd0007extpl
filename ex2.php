<?php

require_once 'lib/tpl.php';

$order_lines = [
    new OrderLine('Pen', 1, true),
    new OrderLine('Paper', 3, false),
    new OrderLine('Staples', 2, true)];

$data = [
    '$sample_message' => 'Hello, templates!'
];

print render_template('templates/ex2_main.html', $data);
